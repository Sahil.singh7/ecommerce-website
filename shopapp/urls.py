from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from shopapp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.homePage,name='home'),
    path('shop',views.offerPage,name='shop'),
    path('contact',views.contactPage,name='contact'),
    path('signup',views.signupPage,name='signup'),
    path('login',views.loginPage,name='login'),
    path('logout',views.logoutPage,name='logout')
]+ static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)