from django.shortcuts import render,HttpResponse,redirect
from .models import Product
from .forms import ContactForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required

# Create your views here.

def signupPage(request):
    if request.method=='POST':
        uname=request.POST.get('username')
        email=request.POST.get('email')
        pass1=request.POST.get('password1')
        pass2=request.POST.get('password2')
        if pass1!=pass2:
            return HttpResponse('<h2>Password does not match</h2>')
        else:
            my_user=User.objects.create_user(uname,email,pass1)
            my_user.save()
            return redirect('login')
    return render(request,'signup.html')


def loginPage(request):
    if request.method=='POST':
        username=request.POST.get('username')
        password=request.POST.get('password')
        my_data=authenticate(request,username=username,password=password)
        if my_data is not None:
            login(request,my_data)
            return redirect('/')
        else:
            return HttpResponse('<h2>Invalid Username or Password</h2>')
    return render(request,'login.html')

login_required(login_url='login') 
def homePage(request):
    products=Product.objects.all()
    print(products)
    return render(request,'homepage.html')

def offerPage(request):
    products=Product.objects.all()
    print(products)
    params={
        'products':products
    }
    return render(request,'shop.html',params)

def contactPage(request):
    form=ContactForm()
    context={
        'form':form
    }
    if request.method=='POST':
        form=ContactForm(request.POST)
        if form.is_valid():
            form.save()
    return render(request,'contact.html',context)

login_required(login_url='login')

def logoutPage(request):
    logout(request)
    return redirect('login')
