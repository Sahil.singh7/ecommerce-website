from django import forms
from django.forms import ModelForm
from .models import *

class ContactForm(forms.ModelForm):
    class Meta:
        model= Contact
        fields='__all__'
        widgets={
            'first_name':forms.TextInput(attrs={'class':'form-control'}),
            'last_name':forms.TextInput(attrs={'class':'form-control'}),
            'email':forms.EmailInput(attrs={'class':'form-control'}),
            'description':forms.Textarea(attrs={'class':'form-control'})
        }
