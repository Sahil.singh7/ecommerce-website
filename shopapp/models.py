from django.db import models

# Create your models here.
class Product(models.Model):
    prod_id=models.IntegerField()
    prod_name=models.CharField(max_length=100)
    prod_desc=models.CharField(max_length=256)
    prod_image=models.ImageField(upload_to='shopapp/images',default='')
    price=models.IntegerField(default=0)


    def __str__(self):                               #all method to be made within class modelName ie. Product
      return self.prod_name


class Contact(models.Model):
   first_name=models.CharField(max_length=50)
   last_name=models.CharField(max_length=50)
   email=models.EmailField(max_length=50)
   description=models.CharField(max_length=256)

